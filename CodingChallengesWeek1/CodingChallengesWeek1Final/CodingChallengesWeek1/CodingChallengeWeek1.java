package CodingChallengesWeek1;

import java.util.ArrayList;
import java.util.Scanner;

public class CodingChallengeWeek1 {

	public static void Login(ArrayList<UserAtributes> user, ArrayList<BookAttributes> arrBook) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName())
						&& passString.equals(user.get(i).getPassword())) {
					UserImpl userImpl = new UserImpl();
					userImpl.Menu(arrBook, user, userString);
				} else {
					tt = false;
				}
			}
			if (tt == false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt == false);
	}

	public static void main(String[] args) {
		ArrayList<BookAttributes> arrBookAttributes = new ArrayList<>();
		arrBookAttributes.add(new BookAttributes(1, "Doraemon", "Fujiko F. Fujio", "truyện tranh"));
		arrBookAttributes.add(new BookAttributes(2, "Conan", "Aoyama Yoshimasa", "truyện tranh"));
		arrBookAttributes.add(new BookAttributes(3, "Shin", "Usui Yoshito ", "truyện tranh"));

		ArrayList<UserAtributes> arruserAtributes = new ArrayList<>();
		ArrayList<Integer> arrFavoriteBookArrayList1 = new ArrayList<>();
		
		arrFavoriteBookArrayList1.add(1);
		arrFavoriteBookArrayList1.add(2);
		arruserAtributes.add(new UserAtributes(1, "user1", "123", "nguyen.thang@hcl.com", arrFavoriteBookArrayList1));
		
		ArrayList<Integer> arrFavoriteBookArrayList2 = new ArrayList<>();
		arrFavoriteBookArrayList2.add(1);
		arrFavoriteBookArrayList2.add(3);
		arruserAtributes.add(new UserAtributes(1, "user2", "123", "nguyen.thang@hcl.com", arrFavoriteBookArrayList2));
		
		ArrayList<Integer> arrFavoriteBookArrayList3 = new ArrayList<>();
		arrFavoriteBookArrayList3.add(2);
		arrFavoriteBookArrayList3.add(3);
		arruserAtributes.add(new UserAtributes(1, "user3", "123", "nguyen.thang@hcl.com", arrFavoriteBookArrayList2));
		
		Login(arruserAtributes, arrBookAttributes);

		
	}

}
