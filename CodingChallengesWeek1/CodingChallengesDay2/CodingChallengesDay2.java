package CodingChallenges;


import java.util.Scanner;

public class CodingChallengesDay2 {
	public static Scanner sc = new Scanner(System.in);
	public static String[] tasks;
	public static void Add() {
		for(int i = 0; i<tasks.length;i++) {
			System.out.print("Task "+(i+1) + ": "); 
			tasks[i] = sc.nextLine();
		}
		
		System.out.println("===========================");
		//Print array
		int c = 0;
		while(c < tasks.length )
		{
			System.out.println("Task "+ (c+1) + ": "+ tasks[c]);
			c++;
		}
	}
	public static void Update() {
		System.out.println("===========================");
		boolean check = true;
		do {
			System.out.print("Enter the task you want to change: ");
			String up = sc.nextLine();
			for(int i = 0; i < tasks.length; i ++) {
				if (tasks[i].toLowerCase().trim().equals(up.toLowerCase().trim())) {
					System.out.print("Enter the change: ");
					tasks[i] = sc.nextLine();
					check = false;
					System.out.println("Update Successful");
					int d = 0;
					while(d < tasks.length )
					{
						System.out.println("Task "+ (d+1) + ": "+ tasks[d]);
						d++;
					}
					
					break;
				}
			
				
			}if (check) System.out.println("No value exists");
		}while(check == true);
		menu();
	}
	public static void Delete() {
		System.out.println("===========================");
		boolean check = true;
		do {
			System.out.print("Enter the task you want to delete: ");
			String del = sc.nextLine();
			for(int i = 0; i < tasks.length; i ++) {
				if (tasks[i].toLowerCase().trim().equals(del.toLowerCase().trim())) {
					tasks[i] = "";
					System.out.print("Delete Successful");
					int d = 0;
					while(d < tasks.length )
					{
						System.out.println("Task "+ (d+1) + ": "+ tasks[d]);
						d++;
					}
					break;
				}
			}
			if (check) System.out.println("No value exists");
		}while(check == true);
		menu();
	}
	public static void Search() {
		System.out.println("===========================");
		boolean check = true;
		do {
			System.out.print("Enter the task you want to search: ");
			String del = sc.nextLine();
			for(int i = 0; i < tasks.length; i ++) {
				if (tasks[i].toLowerCase().trim().equals(del.toLowerCase().trim())) {
					System.out.println("Value exists");
					System.out.println("Task "+ (i+1) + ": "+ tasks[i]);
					break;
				}
			}
			if (check) System.out.println("No value exists");
		}while(check == true);
		menu();
	}
	public static void menu() {
		System.out.println("Menu");
		System.out.println("1. Update ");
		System.out.println("2. Delete ");
		System.out.println("3. Search ");
		System.out.println("0. Exit ");
		System.out.println("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		switch(ch) {
		case 0: break;
		case 1: Update(); break;
		case 2: Delete(); break;
		case 3: Search(); break;
		default: System.out.println("Incorrect input format!");menu();break;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("My name is Nguyen Viet Thang");
		//Input the number of task
		System.out.print("Enter the number of task: ");
		int t = Integer.parseInt(sc.nextLine());
		//Create array
		tasks= new String[t];
		Add();
		menu();
		System.out.println("Finish");
		

	}

}
