package CodingChallengesDay4;

import java.util.ArrayList;
import java.util.Scanner;

public class Visitor {
	public  TaskDAOImp taskImp = new TaskDAOImp();
	public void MenuVisitor(String name, ArrayList<Task> arrTask) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Menu");
		System.out.println("1. Display my task ");
		System.out.println("2. Update ");
		System.out.println("3. Sign out");
		System.out.println("0. Exit ");
		System.out.println("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		switch (ch) {
		case 0:
			break;
		case 1:
			taskImp.DisplayByAssign(name, arrTask);
			MenuVisitor(name, arrTask);
			break;
		case 2:
			taskImp.Update_Task(arrTask);
			MenuVisitor(name, arrTask);
			break;
		case 3:
			CodingChallengesDay4 ngay5 = new CodingChallengesDay4();
			ngay5.Login(arrTask);
			break;
		default:
			CodingChallengesDay4 d5 = new CodingChallengesDay4();
			System.out.println("Incorrect input format!");
			d5.Login(arrTask);
			break;
		}
	}

}
